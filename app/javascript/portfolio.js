function set_positions() {
	$('.card').each((i) => {
		$(this).attr('data-pos', i + 1);
		return;
	});
	return;
}

$(document).ready(function() {
	set_positions();
    $('.sortable').sortable();
    $('.sortable').sortable().bind('sortupdate', (e, ui) => {
    	updated_order = [];
    	set_positions();
		$('.card').each((i, e) => {
			// $(this).attr('data-pos', i + 1);
			updated_order.push({
				id: $(e).data('id'),
				position: i + 1
			})
			return;
		});
		console.log(updated_order);
		$.ajax({
			type: 'PUT',
			url: '/portfolios/sort',
			data: {
				order: updated_order
			}
		});
		return;
    });
    return;
});