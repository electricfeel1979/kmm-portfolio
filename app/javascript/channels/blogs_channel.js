import consumer from "./consumer"

$(document).on('turbolinks:load', function () {
  const comments = $('#comments');
  const channel = "BlogsChannel";
  const blog_id = comments.data('blog-id');

  const global_chat = consumer.subscriptions.create(
	  {
	  	channel,
	  	blog_id,
	  }, 
	  {
		  connected() {
		    // Called when the subscription is ready for use on the server
		    console.log(`connected in channel: ${channel}, blog_id: ${blog_id} `)
		  },

		  disconnected() {
		    // Called when the subscription has been terminated by the server
		  },

		  received(data) {
		    // Called when there's incoming data on the websocket for this channel
		    console.log("Recieving:")
		    console.log(data.content)

		    comments.append(data['comment']);
		  },
		  send_comment(comment, blog_id) {
		  	this.perform('send_comment', {
		  		comment,
		  		blog_id,
		  	});
		  }
  });

  $('#new_comment').submit((e) => {
    let $this, textarea;
    $this = $(this);
    textarea = $this.find('#comment_content');
    if ($.trim(textarea.val()).length > 1) {
      global_chat.send_comment(textarea.val(), comments.data('blog-id'));
      textarea.val('');
    }
    e.preventDefault();
    return false;
   });
});


