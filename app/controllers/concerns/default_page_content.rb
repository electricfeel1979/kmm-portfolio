module DefaultPageContent
	extend ActiveSupport::Concern

	included do
		before_action :set_page_defaults
	end


	def set_page_defaults
		@page_title = "Kevin Mark Montesclaros | My Portfolio Website"
		@seo_keywords = "Kevin Mark Montesclaros Portfolio"
	end
end