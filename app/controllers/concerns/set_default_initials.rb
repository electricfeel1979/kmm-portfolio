module SetDefaultInitials
	extend ActiveSupport::Concern

	included do
		before_action :set_initials
	end


	def set_initials
      begin
         @video_bg_url = ActionController::Base.helpers.video_path("bgvideo.mp4")
      rescue => e
        @video_bg_url = "https://kmm-portfolio-bucket.s3-ap-southeast-1.amazonaws.com/app_videos/bgvideo.mp4"
      end
	end
end